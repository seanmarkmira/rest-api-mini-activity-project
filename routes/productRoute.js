const express = require('express')
const router = express.Router()
const {getAllProducts, createProducts, getSpecProduct, deleteProduct, deleteAllProduct} = require('../controller/products_controller')

router.route('/').get(getAllProducts)
router.route('/create').post(createProducts)
router.route('/:id').get(getSpecProduct)
router.route('/delete/:id').delete(deleteProduct)
router.route('/deleteall').delete(deleteAllProduct)

module.exports = router