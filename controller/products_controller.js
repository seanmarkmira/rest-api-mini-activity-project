const Product = require('../model/products')

const getAllProducts = (req,res) =>{
	return Product.find({}).then(result=>{
		return res.json(result)
	})
}

const createProducts = (req,res)=>{
	Product.findOne({name:req.body.name},(err,result)=>{
		if(result){
			console.log('there is a hit')
			console.log(result)
			res.status(400).json({status:false, msg:"Duplicate product"})
			
		}else{
			let newProduct = new Product({
				name: req.body.name,
				price: req.body.price
			})

			newProduct.save().then((result, error)=>{
				if(error){
					console.log(err)
				}else{
					return res.status(200).json(result)
				}
			})
		}
	})
}

const getSpecProduct = (req,res)=>{
	Product.findById(req.params.id).then((result,error)=>{
		if(error){
			console.log(error)
		}else{
			res.status(200).json(result)
		}
	})
}

const deleteProduct = (req,res)=>{
	console.log('in delete')
	Product.findById(req.params.id).then((result,error)=>{
		if(error){
			console.log(error)
		}else{
			Product.deleteOne({_id:req.params.id}).then(function(){
				console.log('deleted')
				console.log(result._doc)
			}).catch(function(error){
				console.log(error)
			})
			return res.json({...result._doc,status:"deleted"})
		}
	})
}

const deleteAllProduct = (req,res)=>{
	Product.deleteMany().then(function(){
		console.log('DELETED ALL ENTRIES')
	}).catch(function(error){
		console.log(error)
	})
	res.status(200).json({success:true, msg:"delete all entries"})
}

module.exports = {
	getAllProducts,
	createProducts,
	getSpecProduct,
	deleteAllProduct,
	deleteProduct
}	
