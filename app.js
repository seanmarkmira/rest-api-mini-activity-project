const express = require('express')
const mongoose = require('mongoose')
const productRoute = require('./routes/productRoute.js')
const port = 3599;
const app = express();
require('dotenv').config()

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/products', productRoute)

mongoose.connect(process.env.MONGO_URI,{
	useNewUrlParser:true,
	useUnifiedTopology:true
})

app.listen(port, ()=>{
	console.log('connected to port 3599')
})